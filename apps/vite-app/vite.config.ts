import { defineConfig } from 'vite'

export default defineConfig({
    server: {
        fs: {
            allow: [
            './src/',
            './media/',
            './video_slides/',
            './node_modules/',
            '/Users/lorenzo/.yarn/berry/cache/',
            ],
        },
    },
})