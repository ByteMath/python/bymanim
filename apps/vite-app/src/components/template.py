from .imports import *

temp = TexTemplate()
temp.add_to_preamble(r"""
    \usepackage{stmaryrd,mathtools,marvosym,fontawesome}
    \newcommand{\comm}[2]     {\left\llbracket#1,#2\right\rrbracket}
    \newcommand{\bra}[1]      {\left\langle #1\right|}
    \newcommand{\ket}[1]      {\left|#1\right\rangle}
    \newcommand{\braket}[2]   {\left\langle #1\middle|#2\right\rangle}
    \newcommand{\ketbra}[2]   {\left|#1\middle\rangle\!\middle\langle#2\right|}
    \newcommand{\braopket}[3] {\left\langle #1\middle|#2\middle|#3\right\rangle}
    \newcommand{\proj}[1]     {\left| #1\middle\rangle\!\middle\langle#1\right|}
    \newcommand{\abs}[1]      {\left| #1 \right|}
    \newcommand{\norm}[1]     {\left\| #1 \right\|}
    \newcommand{\Tr}          {\mathrm{Tr}}
""")
temp.add_to_document(r"""
    \fontfamily{lmss}\selectfont
""")

def MyTex(*x,tex_environment="center",color=WHITE):
    return Tex(*x,
        tex_template=temp,
        tex_environment=tex_environment,
        color=color
    )

def MyMathTex(*x,tex_environment="align*",color=WHITE):
    return MyTex(*x,
        tex_environment=tex_environment,
        color=color
    )

def OffsetBezier(p1,o1,p2,o2,*x):
    return CubicBezier(
        p1,p1+o1,p2+o2,p2,*x)

