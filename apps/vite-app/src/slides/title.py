

class Title(SlideScene):
    def construct(self):
        title = MyTex(r"\bfseries\textsc{Title}").scale(1.25).shift(2.5*UP)
        arxiv = MyTex(r"\bfseries\texttt{arXiv:????.?????}").scale(.75).shift(1.5*UP)
        name = MyTex("Christopher T.\ Chubb")
        ethz=SVGMobject("ethz_logo_white.svg").scale(1/3).next_to(1.5*DOWN,LEFT,buff=1.5)
        udes=SVGMobject("Université_de_Sherbrooke_(logo).svg").scale(1/3).next_to(1.5*DOWN,RIGHT,buff=1.5)
        footer_big=footer.copy().arrange(RIGHT,buff=.375).to_corner(DOWN).shift(0.25*UP).scale(1.25).set_opacity(1)

        self.add(name,title,arxiv,ethz,udes,footer_big)
        
        self.play(Unwrite(title),Unwrite(arxiv),Unwrite(name),Unwrite(ethz),Unwrite(udes))
        self.play(ReplacementTransform(footer_big,footer))
        self.wait()
        self.play(FadeIn(toc))
        self.slide_break()

        self.play(toc[0].animate.scale(1.2).set_color(YELLOW))
        self.slide_break()

        for i in range(1,len(toc)):
            self.play(
                toc[i].animate.scale(1.2).set_color(YELLOW),
                toc[i-1].animate.scale(1/1.2).set_color(WHITE),
            )
            self.slide_break()

        self.play(toc[-1].animate.scale(1/1.2).set_color(WHITE))