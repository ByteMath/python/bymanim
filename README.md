# `ByMaNiM` 

`ByMaNiM` is a framework for creating manim-reveal.js slides.


## What's inside?

This repo includes the following packages and apps:

### Apps and Packages

- `vite-app`: a vanilla [vite](https://vitejs.dev) ts app example with a basic presentation.
- `@bymanim/py`: a python package with components and tools for creating manim presentation.
- `@repo/eslint-config`: shared `eslint` configurations
- `@repo/typescript-config`: `tsconfig.json`s used throughout the monorepo

Each package and app is 100% [TypeScript](https://www.typescriptlang.org/).

### Utilities

This Turborepo has some additional tools already setup for you:

- [TypeScript](https://www.typescriptlang.org/) for static type checking
- [ESLint](https://eslint.org/) for code linting
- [Prettier](https://prettier.io) for code formatting
